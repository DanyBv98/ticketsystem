package eu.danybv.tw.ticketsystem.utils.server;

import org.jooq.tools.json.JSONObject;

import java.util.List;

public class DStringUtils {

    public static <T> String listToString(List<T> list)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for(var element : list)
            stringBuilder.append(element.toString() + '\n');
        return stringBuilder.toString();
    }

    public static String generateJsonMessage(boolean success, String message, Object data)
    {
        JSONObject object = new JSONObject();
        object.put("success", success);
        object.put("message", message);
        object.put("data", data);
        return object.toString();
    }

    public static String generateJsonMessage(boolean success, String message)
    {
        return generateJsonMessage(success, message, null);
    }
}
