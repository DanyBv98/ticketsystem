package eu.danybv.tw.ticketsystem.utils.server;

import eu.danybv.tw.ticketsystem.models.Ticket;
import eu.danybv.tw.ticketsystem.models.User;
import org.jooq.impl.DSL;

import java.util.List;

public class TicketUtils {

    public static boolean createTicket(User user, String title, String description) {
        Ticket ticket = new Ticket();
        ticket.setTitle(title);
        ticket.setDescription(description);
        ticket.setUser(user);
        ticket.save();
        return true;
    }

    public static List<Ticket> showTickets(User user) {
        var sampleTicket = new Ticket();
        if (user.isAdmin()) return sampleTicket.<Ticket>all();
        return sampleTicket.<Ticket>select(sampleTicket.selectQuery().where(DSL.field("userId").eq(user.getId())));
    }

    public static boolean assignTicket(User user, long ticketId) {
        var ticket = getTicket(ticketId);
        if(ticket == null) return false;
        if(ticket.getAssignedUser() != null) return false;
        ticket.setAssignedUser(user);
        ticket.save();
        return true;
    }

    public static Ticket getTicket(long ticketId)
    {
        var sampleTicket = new Ticket();
        var result = sampleTicket.<Ticket>select(sampleTicket.selectQuery().where(DSL.field("id").eq(ticketId)));
        if(result.isEmpty())
            return null;
        return result.get(0);
    }

    public static boolean ticketExists(long ticketId)
    {
        return getTicket(ticketId) != null;
    }

    public static boolean changeTicketStatus(User user, long ticketId, String newStatus)
    {
        var ticket = getTicket(ticketId);
        if(ticket == null) return false;
        if(ticket.getAssignedUser() == null || ticket.getAssignedUser().getId() != user.getId()) return false;
        ticket.setStatus(newStatus);
        ticket.save();
        return true;
    }
}
