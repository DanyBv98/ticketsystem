package eu.danybv.tw.ticketsystem.utils.server;

import eu.danybv.tw.ticketsystem.models.User;
import org.jooq.impl.DSL;
import org.mindrot.jbcrypt.BCrypt;

public class LoginUtils {

    public static boolean register(String username, String password) {
        if(usernameExists(username)) return false;
        User user = new User();
        user.setUsername(username);
        user.setPassword(hashPassword(password));
        user.save();
        return true;
    }

    public static boolean login(String username, String password) {
        var user = getUserByUsername(username);
        if(user == null) return false;
        return isPasswordCorrect(password, user.getPassword());
    }

    public static User getUserByUsername(String username)
    {
        User sampleUser = new User();
        var result = sampleUser.<User>select(sampleUser.selectQuery().where(DSL.field("username").eq(username)));
        if(result.isEmpty())
            return null;
        return result.get(0);
    }

    public static boolean usernameExists(String username) {
        return getUserByUsername(username) != null;
    }

    public static String hashPassword(String password)
    {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean isPasswordCorrect(String plainPassword, String hashedPassword)
    {
        return BCrypt.checkpw(plainPassword, hashedPassword);
    }
}
