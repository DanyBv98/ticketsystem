package eu.danybv.tw.ticketsystem.utils.client;

import eu.danybv.tw.ticketsystem.cli.ServerResponse;
import eu.danybv.tw.ticketsystem.networking.Client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClientHelper {

    public static ServerResponse request(Client client, String command, List<String> arguments) {
        try {
            client.send(command);
            for (var argument : arguments)
                client.send(argument);
            client.flush();
            return ServerResponse.parse(client.receive());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ServerResponse login(Client client, String username, String password) {
        var arguments = new ArrayList<String>();
        arguments.add(username);
        arguments.add(password);
        return request(client, "login", arguments);
    }

    public static ServerResponse register(Client client, String username, String password) {
        var arguments = new ArrayList<String>();
        arguments.add(username);
        arguments.add(password);
        return request(client, "register", arguments);
    }

    public static ServerResponse createTicket(Client client, String title, String description) {
        var arguments = new ArrayList<String>();
        arguments.add(title);
        arguments.add(description);
        return request(client, "create_ticket", arguments);
    }

    public static ServerResponse showTickets(Client client) {
        var arguments = new ArrayList<String>();
        return request(client, "show_tickets", arguments);
    }

    public static ServerResponse checkTicket(Client client, long ticketId) {
        var arguments = new ArrayList<String>();
        arguments.add(ticketId + "");
        return request(client, "check_ticket", arguments);
    }

    public static ServerResponse changeTicket(Client client, long ticketId, String status) {
        var arguments = new ArrayList<String>();
        arguments.add(ticketId + "");
        arguments.add(status);
        return request(client, "change_ticket", arguments);
    }

    public static ServerResponse assignTicket(Client client, long ticketId) {
        var arguments = new ArrayList<String>();
        arguments.add(ticketId + "");
        return request(client, "assign_ticket", arguments);
    }
}
