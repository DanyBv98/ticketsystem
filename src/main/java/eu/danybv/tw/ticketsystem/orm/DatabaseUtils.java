package eu.danybv.tw.ticketsystem.orm;

import java.sql.*;
import java.util.*;

public class DatabaseUtils {

    private static Map<Class, String> typeToSqlType = Map.ofEntries(
            Map.entry(boolean.class, "BOOLEAN"),
            Map.entry(Boolean.class, "BOOLEAN"),
            Map.entry(short.class, "SMALLINT"),
            Map.entry(Short.class, "SMALLINT"),
            Map.entry(int.class, "INTEGER"),
            Map.entry(Integer.class, "INTEGER"),
            Map.entry(long.class, "BIGINT"),
            Map.entry(Long.class, "BIGINT"),
            Map.entry(float.class, "REAL"),
            Map.entry(Float.class, "REAL"),
            Map.entry(double.class, "DOUBLE"),
            Map.entry(Double.class, "DOUBLE"),
            Map.entry(String.class, "VARCHAR"),
            Map.entry(Clob.class, "CLOB"),
            Map.entry(Timestamp.class, "TIMESTAMP")
    );

    public static String getSqlType(Class type)
    {
        if(!typeToSqlType.containsKey(type)) return "";
        return typeToSqlType.get(type);
    }

    public static List<SortedMap<String, String>> resultSetToMapList(ResultSet resultSet) throws SQLException {
        var result = new ArrayList<SortedMap<String, String>>();
        ResultSetMetaData metaData = resultSet.getMetaData();

        var columnsNumber = metaData.getColumnCount();
        while (resultSet.next()) {
            var currentRow = new TreeMap<String, String>();
            for (int i = 1; i <= columnsNumber; i++) {
                currentRow.put(metaData.getColumnName(i), resultSet.getString(i));
            }
            result.add(currentRow);
        }

        return result;
    }
}
