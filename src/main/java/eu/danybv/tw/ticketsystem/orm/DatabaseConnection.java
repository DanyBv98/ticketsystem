package eu.danybv.tw.ticketsystem.orm;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DatabaseConnection {

    private static Connection connection;
    public static final String kCatalogName = "TICKETSYSTEM";
    public static final String kSchemaName = "PUBLIC";

    public static void init() throws SQLException {
        if(connection != null && !connection.isClosed()) return;
        connection = DriverManager.getConnection("jdbc:h2:~/dbs/ticketsystem", "root", "1w3w5w7w9");
    }

    public static Connection get()
    {
        try {
            if(connection == null || connection.isClosed())
                init();
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void close() throws SQLException {
        connection.close();
    }


    public static Map<String, String> tables() throws SQLException {
        var tableResultSet = DatabaseConnection.get().getMetaData()
                .getTables(kCatalogName, kSchemaName, null, null);

        Map<String, String> tables = new HashMap<String, String>();
        while(tableResultSet.next())
        {
            tables.put(tableResultSet.getString("TABLE_NAME"), tableResultSet.getString("SQL"));
        }
        return tables;
    }

    public static DSLContext dslContext() {
        return DSL.using(get(), SQLDialect.H2);
    }

}
