package eu.danybv.tw.ticketsystem.orm;

import eu.danybv.tw.ticketsystem.orm.annotations.*;
import eu.danybv.tw.ticketsystem.orm.annotations.ForeignKey;
import eu.danybv.tw.ticketsystem.orm.annotations.Identity;
import eu.danybv.tw.ticketsystem.orm.exceptions.NoTableNameSetException;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SQLModel implements IModel {

    public static long kInvalidIdentity = -1;

    public java.lang.reflect.Field[] getPropertyFields()
    {
        return Arrays.stream(getClass().getDeclaredFields()).filter(f -> f.getAnnotation(DBProperty.class) != null).toArray(java.lang.reflect.Field[]::new);
    }

    public String getSequenceName(java.lang.reflect.Field field)  {
        return "S_" + getTableName().toUpperCase() + "_" + getFieldName(field).toUpperCase();
    }

    @Override
    public String getTableName() {
        var modelAnnotation = getClass().getAnnotation(Model.class);
        if(modelAnnotation == null) return null;
        var tableName = modelAnnotation.tableName();
        if(tableName == null || tableName.isBlank()) return null;
        return tableName;
    }

    @Override
    public void createTable() throws NoTableNameSetException {
        var tableName = getTableName();
        try(var connection = DatabaseConnection.get();) {
            var statement = connection.createStatement();

            String sql = "CREATE TABLE IF NOT EXISTS " + tableName + "(";
            var foreignKeys = new ArrayList<String>();

            int propertyIndex = 0;
            /**
             * You may wonder why did I choose to use a foreach and make an index,
             * instead of using a normal for. It's quite easy actually, I chose to
             * do this, because with a normal for the index will increment even through
             * fields that are not meant to be SQL columns (those without annotations).
             * This way, I can control when that happens.
             */
            for(var field : getPropertyFields())
            {
                var fieldName = getFieldName(field);
                var sqlType = DatabaseUtils.getSqlType(field.getType());

                if(field.getAnnotation(Identity.class) != null)
                    sqlType = "IDENTITY";
                if(field.getAnnotation(AutoIncrement.class) != null)
                    sqlType += " AUTO_INCREMENT ";
                if(field.getAnnotation(NotNull.class) != null)
                    sqlType += " NOT NULL ";
                if(field.getAnnotation(PrimaryKey.class) != null)
                    sqlType += " PRIMARY KEY ";
                if(field.getAnnotation(ForeignKey.class) != null)
                {
                    var foreignKeyAnnotation = field.getAnnotation(ForeignKey.class);
                    var modelAnnotation = foreignKeyAnnotation.model().getDeclaredAnnotation(Model.class);
                    if(modelAnnotation == null) throw new NoTableNameSetException();
                    foreignKeys.add("FOREIGN KEY (" + field.getName() + ")" + " REFERENCES " + modelAnnotation.tableName() +  "(" + foreignKeyAnnotation.column() + ")");
                }

                if(propertyIndex > 0)
                    sql += ", ";

                sql += fieldName + " " + sqlType;

                propertyIndex++;
            }

            for(var foreignKey : foreignKeys)
                sql += ", " + foreignKey;
            sql += ")";

            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void save() {
        var columns = new ArrayList<org.jooq.Field<?>>();
        var keys = new ArrayList<org.jooq.Field<?>>();
        var values = new ArrayList<Object>();

        boolean insert = false;

        for(var field : getPropertyFields())
        {
            try {
                field.setAccessible(true);
                var dslField = DSL.field(getFieldName(field).toUpperCase());
                var fieldValue = field.get(this);

                if(field.getAnnotation(Identity.class) != null)
                {
                    keys.add(dslField);
                    if(field.getLong(this) == kInvalidIdentity)
                    {
                        insert = true;
                        continue;
                    }
                }

                columns.add(dslField);
                values.add(fieldValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        var table = DSL.table(DSL.name(DatabaseConnection.kSchemaName, getTableName().toUpperCase()));
        var context = DatabaseConnection.dslContext();
        if(insert)
        {
            context.insertInto(table).columns(columns).values(values).execute();
        }
        else
        {
            context.mergeInto(table).columns(columns).key(keys).values(values).execute();
        }
    }

    public <T extends SQLModel> List<T> all()
    {
        return select(selectQuery());
    }

    public SelectJoinStep<? extends Record> selectQuery()
    {
        return DatabaseConnection.dslContext().select().from(getTableName());
    }

    public <T extends SQLModel> List<T> select(Select<? extends Record> select) {
        List<T> list = new ArrayList<T>();
        var records = select.fetchMaps();
        var fields = getPropertyFields();

        for(var record : records)
        {
            try {
                var instance = getClass().getDeclaredConstructor().newInstance();
                for(var fieldName : record.keySet())
                {
                    var field = Arrays.stream(fields).filter(f -> f.getName().equalsIgnoreCase(fieldName) ||
                            (f.getAnnotation(DBProperty.class) != null && f.getAnnotation(DBProperty.class).name().equalsIgnoreCase(fieldName))).findFirst();
                    if(field.isEmpty()) continue;
                    field.get().setAccessible(true);
                    field.get().set(instance, record.get(fieldName));
                }
                list.add((T) instance);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    private String getFieldName(java.lang.reflect.Field field)
    {
        String name = field.getName();
        var dbProperty = field.getAnnotation(DBProperty.class);
        if(dbProperty != null && !dbProperty.name().isEmpty())
            name = dbProperty.name();
        return name;
    }

}
