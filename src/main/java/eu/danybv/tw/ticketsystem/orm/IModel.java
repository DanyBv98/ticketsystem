package eu.danybv.tw.ticketsystem.orm;

import eu.danybv.tw.ticketsystem.orm.exceptions.NoTableNameSetException;

public interface IModel {
    public String getTableName();
    void createTable() throws NoTableNameSetException;
    void save();
}
