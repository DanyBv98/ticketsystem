package eu.danybv.tw.ticketsystem;

import eu.danybv.tw.ticketsystem.models.Ticket;
import eu.danybv.tw.ticketsystem.models.User;
import eu.danybv.tw.ticketsystem.orm.IModel;
import eu.danybv.tw.ticketsystem.orm.exceptions.NoTableNameSetException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class DatabaseHelper {

    public static void createTables()
    {
        var models = new ArrayList<Class<? extends IModel>>();
        models.add(User.class);
        models.add(Ticket.class);

        for(var modelClass : models)
        {
            try {
                var instance = modelClass.getDeclaredConstructor().newInstance();
                instance.createTable();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | NoTableNameSetException e) {
                e.printStackTrace();
            }
        }
    }
}
