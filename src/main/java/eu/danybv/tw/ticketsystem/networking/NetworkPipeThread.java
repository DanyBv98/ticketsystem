package eu.danybv.tw.ticketsystem.networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class NetworkPipeThread extends Thread implements INetworkPipe {
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    public NetworkPipeThread()
    {
        this.inputStream = null;
        this.outputStream = null;
    }

    public NetworkPipeThread(ObjectInputStream inputStream, ObjectOutputStream outputStream)
    {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    @Override
    public String receive() throws IOException {
        return inputStream.readUTF();
    }

    @Override
    public void send(String data) throws IOException {
        outputStream.writeUTF(data);
    }

    @Override
    public void flush() throws IOException {
        outputStream.flush();
    }

    protected void setInputStream(ObjectInputStream inputStream) {
        this.inputStream = inputStream;
    }

    protected void setOutputStream(ObjectOutputStream outputStream) {
        this.outputStream = outputStream;
    }
}
