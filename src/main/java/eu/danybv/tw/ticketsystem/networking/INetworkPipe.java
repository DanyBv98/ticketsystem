package eu.danybv.tw.ticketsystem.networking;

import java.io.IOException;

public interface INetworkPipe {
    String receive() throws IOException;
    void send(String data) throws IOException;
    void flush() throws IOException;
}
