package eu.danybv.tw.ticketsystem.networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client extends NetworkPipeThread {

    private Socket socket;

    public Client() {
        try {
            socket = new Socket(Server.kHostname, Server.kPort);
            setOutputStream(new ObjectOutputStream(socket.getOutputStream()));
            setInputStream(new ObjectInputStream(socket.getInputStream()));
            start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
