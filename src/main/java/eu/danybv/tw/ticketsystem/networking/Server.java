package eu.danybv.tw.ticketsystem.networking;

import java.io.IOException;
import java.net.ServerSocket;

public class Server extends Thread {
    private ServerSocket server;

    public static final String kHostname = "localhost";
    public static final int kPort = 14329;

    public void run()
    {
        try {
            server = new ServerSocket(kPort);
            while(true)
            {
                var socket = server.accept();
                if(socket == null) continue;
                var client = new ServerClientThread(socket);
                client.start();
                System.out.println(client.getSocket().getInetAddress().getHostAddress() + " has connected.");
            }
        } catch (IOException exception) {
        }
    }


}
