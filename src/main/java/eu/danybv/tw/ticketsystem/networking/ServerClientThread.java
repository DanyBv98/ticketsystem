package eu.danybv.tw.ticketsystem.networking;

import eu.danybv.tw.ticketsystem.commands.CommandExecutor;
import eu.danybv.tw.ticketsystem.models.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ServerClientThread extends NetworkPipeThread {
    private Socket socket;

    private User user = null;

    public ServerClientThread(Socket socket)
    {
        try {
            this.socket = socket;
            setInputStream(new ObjectInputStream(socket.getInputStream()));
            setOutputStream(new ObjectOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run()
    {
        try {
            while(true)
            {
                String command = receive();
                CommandExecutor.execute(this, command);
            }
        } catch(SocketException e) {
            //e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
