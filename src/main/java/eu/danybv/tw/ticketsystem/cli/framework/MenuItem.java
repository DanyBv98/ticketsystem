package eu.danybv.tw.ticketsystem.cli.framework;

import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

public class MenuItem {
    private String title;
    private BooleanSupplier action;

    public MenuItem(String title, BooleanSupplier action)
    {
        this.title = title;
        this.action = action;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BooleanSupplier getAction() {
        return action;
    }

    public void setAction(BooleanSupplier action) {
        this.action = action;
    }
}
