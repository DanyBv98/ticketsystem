package eu.danybv.tw.ticketsystem.cli.framework;

import java.util.Stack;

public class MenuStateMachine {

    private static Stack<Menu> menus;

    static {
        menus = new Stack<>();
    }

    public static boolean show()
    {
        if(menus.empty()) return true;
        menus.peek().showMenu();
        return false;
    }

    public static void pop()
    {
        menus.pop();
    }

    public static void push(Menu menu)
    {
        menus.push(menu);
    }
}
