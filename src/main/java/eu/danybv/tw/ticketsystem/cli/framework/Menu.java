package eu.danybv.tw.ticketsystem.cli.framework;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private Map<Integer, MenuItem> commandsMap = new LinkedHashMap<>();

    protected void registerCommand(int key, MenuItem menuItem)
    {
        commandsMap.put(key, menuItem);
    }

    public boolean showMenu(PrintStream out, InputStream in)
    {
        for(var key : commandsMap.keySet())
        {
            var menuItem = commandsMap.get(key);
            out.println(key + ". " + menuItem.getTitle());
        }

        Scanner scanner = new Scanner(in);

        var isOptionValid = true;
        int option = 0;
        try {
            option = scanner.nextInt();
            isOptionValid = commandsMap.containsKey(option);
        } catch(InputMismatchException exception) {
            isOptionValid = false;
        }
        if(!isOptionValid)
        {
            out.println("Invalid option.");
            return false;
        }
        return commandsMap.get(option).getAction().getAsBoolean();
    }

    public boolean showMenu()
    {
        return showMenu(System.out, System.in);
    }
}
