package eu.danybv.tw.ticketsystem.cli.menus;

import eu.danybv.tw.ticketsystem.cli.framework.Menu;
import eu.danybv.tw.ticketsystem.cli.framework.MenuItem;
import eu.danybv.tw.ticketsystem.cli.framework.MenuStateMachine;
import eu.danybv.tw.ticketsystem.networking.Client;
import eu.danybv.tw.ticketsystem.utils.client.ClientHelper;

import java.util.Scanner;

public class FrontPageMenu extends Menu {

    private Client client;
    public FrontPageMenu(Client client)
    {
        this.client = client;
        registerCommand(1, new MenuItem("Login", ()->{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Username: ");
            var username = scanner.nextLine();
            System.out.println("Password: ");
            var password = scanner.nextLine();
            var serverResponse = ClientHelper.login(client, username, password);
            System.out.println(serverResponse.getMessage());
            if(serverResponse.isSuccess())
            {
                var isAdmin = (boolean) serverResponse.getData();
                if(isAdmin)
                {
                    MenuStateMachine.push(new AdminMenu(client));
                }
                else
                {
                    MenuStateMachine.push(new UserMenu(client));
                }
            }
            return serverResponse.isSuccess();
        }));
        registerCommand(2, new MenuItem("Register", ()->{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Username: ");
            var username = scanner.nextLine();
            System.out.println("Password: ");
            var password = scanner.nextLine();
            var serverResponse = ClientHelper.register(client, username, password);
            System.out.println(serverResponse.getMessage());
            return false;
        }));
        registerCommand(0, new MenuItem("Exit", ()->{
            MenuStateMachine.pop();
            return true;
        }));
    }

}
