package eu.danybv.tw.ticketsystem.cli.menus;

import eu.danybv.tw.ticketsystem.cli.framework.Menu;
import eu.danybv.tw.ticketsystem.cli.framework.MenuItem;
import eu.danybv.tw.ticketsystem.cli.framework.MenuStateMachine;
import eu.danybv.tw.ticketsystem.networking.Client;
import eu.danybv.tw.ticketsystem.utils.client.ClientHelper;

import java.util.InputMismatchException;
import java.util.Scanner;

public class AdminMenu extends Menu {
    private Client client;

    public AdminMenu(Client client) {
        registerCommand(1, new MenuItem("Show tickets", () -> {
            var serverResponse = ClientHelper.showTickets(client);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(2, new MenuItem("Assign ticket", () -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ticket ID: ");
            long ticketId;
            try {
                ticketId = scanner.nextLong();
            } catch (InputMismatchException ex) {
                System.out.println("Please enter a valid id.");
                return true;
            }
            var serverResponse = ClientHelper.assignTicket(client, ticketId);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(3, new MenuItem("Change ticket status", () -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ticket ID: ");
            long ticketId;
            try {
                ticketId = scanner.nextLong();
            } catch (InputMismatchException ex) {
                System.out.println("Please enter a valid id.");
                return true;
            }
            System.out.println("Status: ");
            var status = scanner.next();
            var serverResponse = ClientHelper.changeTicket(client, ticketId, status);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(4, new MenuItem("Check ticket", () -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ticket ID: ");
            long ticketId;
            try {
                ticketId = scanner.nextLong();
            } catch (InputMismatchException ex) {
                System.out.println("Please enter a valid id.");
                return true;
            }
            var serverResponse = ClientHelper.checkTicket(client, ticketId);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(0, new MenuItem("Logout", () -> {
            MenuStateMachine.pop();
            return true;
        }));
    }
}
