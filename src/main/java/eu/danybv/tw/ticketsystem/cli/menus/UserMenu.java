package eu.danybv.tw.ticketsystem.cli.menus;

import eu.danybv.tw.ticketsystem.cli.framework.Menu;
import eu.danybv.tw.ticketsystem.cli.framework.MenuItem;
import eu.danybv.tw.ticketsystem.cli.framework.MenuStateMachine;
import eu.danybv.tw.ticketsystem.networking.Client;
import eu.danybv.tw.ticketsystem.utils.client.ClientHelper;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserMenu extends Menu {

    private Client client;
    public UserMenu(Client client) {
        registerCommand(1, new MenuItem("Create ticket", () -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Title: ");
            var title = scanner.nextLine();
            System.out.println("Description: ");
            var description = scanner.nextLine();
            var serverResponse = ClientHelper.createTicket(client, title, description);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(2, new MenuItem("Show your tickets", () -> {
            var serverResponse = ClientHelper.showTickets(client);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(3, new MenuItem("Check ticket", () -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ticket ID: ");
            long ticketId;
            try {
                ticketId = scanner.nextLong();
            } catch (InputMismatchException ex) {
                System.out.println("Please enter a valid id.");
                return true;
            }
            var serverResponse = ClientHelper.checkTicket(client, ticketId);
            System.out.println(serverResponse.getMessage());
            return true;
        }));
        registerCommand(0, new MenuItem("Logout", () -> {
            MenuStateMachine.pop();
            return true;
        }));
    }
}
