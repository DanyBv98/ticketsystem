package eu.danybv.tw.ticketsystem.cli;

import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import org.jooq.tools.json.ParseException;

public class ServerResponse {
    private boolean success;
    private String message;
    private Object additionalData;

    public ServerResponse(boolean success, String message) {
        this(success, message, null);
    }

    public ServerResponse(boolean success, String message, Object additionalData) {
        this.success = success;
        this.message = message;
        this.additionalData = additionalData;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return additionalData;
    }

    public static ServerResponse parse(String response)
    {
        var success = true;
        var message = response;
        Object additionalData = null;

        JSONObject json = null;
        try {
            json = (JSONObject) new JSONParser().parse(response);
            success = (boolean) json.get("success");
            message = (String) json.get("message");
            if(json.containsKey("data"))
                additionalData = json.get("data");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ServerResponse(success, message, additionalData);
    }
}
