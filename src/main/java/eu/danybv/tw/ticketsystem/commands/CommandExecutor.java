package eu.danybv.tw.ticketsystem.commands;

import eu.danybv.tw.ticketsystem.networking.ServerClientThread;
import eu.danybv.tw.ticketsystem.orm.SQLModel;
import eu.danybv.tw.ticketsystem.utils.server.DStringUtils;
import eu.danybv.tw.ticketsystem.utils.server.LoginUtils;
import eu.danybv.tw.ticketsystem.utils.server.TicketUtils;

import java.io.IOException;

public class CommandExecutor {

    private static boolean loggedInMiddleware(ServerClientThread client) throws IOException {
        if(client.getUser() == null)
        {
            client.send(DStringUtils.generateJsonMessage(false, "You must be logged in to do that."));
            return false;
        }
        return true;
    }

    private static boolean adminMiddleware(ServerClientThread client) throws IOException {
        if(!loggedInMiddleware(client)) return false;
        if(!client.getUser().isAdmin())
        {
            client.send(DStringUtils.generateJsonMessage(false, "You must be admin to do that."));
            return false;
        }
        return true;
    }

    private static long readTicketId(ServerClientThread client) throws IOException {
        long ticketId = SQLModel.kInvalidIdentity;
        try{ ticketId = Long.parseLong(client.receive()); } catch(NumberFormatException ex) { }
        if(!TicketUtils.ticketExists(ticketId))
        {
            ticketId = SQLModel.kInvalidIdentity;
            client.send(DStringUtils.generateJsonMessage(false, "There is no ticket with that id."));
        }
        return ticketId;
    }

    public static void execute(ServerClientThread client, String command) throws IOException {
        switch (command) {
            case "register": {
                var username = client.receive();
                var password = client.receive();
                if (LoginUtils.register(username, password)) {
                    client.send(DStringUtils.generateJsonMessage(true, "Registration successfully."));
                } else {
                    client.send(DStringUtils.generateJsonMessage(false, "Username already exists."));
                }
                break;
            }
            case "login": {
                var username = client.receive();
                var password = client.receive();
                if (LoginUtils.login(username, password)) {
                    var user = LoginUtils.getUserByUsername(username);
                    client.send(DStringUtils.generateJsonMessage(true, "Login successfully.", user.isAdmin()));
                    client.setUser(user);
                } else {
                    client.send(DStringUtils.generateJsonMessage(false, "Invalid credentials."));
                }
                break;
            }
            case "show_tickets": {
                if(!loggedInMiddleware(client)) break;
                var tickets = TicketUtils.showTickets(client.getUser());
                client.send(DStringUtils.generateJsonMessage(true, DStringUtils.listToString(tickets)));
                break;
            }
            case "create_ticket": {
                if(!loggedInMiddleware(client)) break;
                var title = client.receive();
                var description = client.receive();
                TicketUtils.createTicket(client.getUser(), title, description);
                client.send(DStringUtils.generateJsonMessage(true, "Successfully created ticket."));
                break;
            }
            case "assign_ticket": {
                if(!adminMiddleware(client)) break;
                long ticketId = readTicketId(client);
                if(ticketId == SQLModel.kInvalidIdentity) break;
                var result = TicketUtils.assignTicket(client.getUser(), ticketId);
                if(result)
                {
                    client.send(DStringUtils.generateJsonMessage(true, "Assigned ticket."));
                }
                else
                {
                    client.send(DStringUtils.generateJsonMessage(false, "Ticket already assigned."));
                }
                break;
            }
            case "check_ticket": {
                if(!loggedInMiddleware(client)) break;
                long ticketId = readTicketId(client);
                if(ticketId == SQLModel.kInvalidIdentity) break;
                var ticket = TicketUtils.getTicket(ticketId);
                if(ticket.getUser().getId() != client.getUser().getId() && !client.getUser().isAdmin())
                {
                    client.send(DStringUtils.generateJsonMessage(false, "That is not your ticket."));
                }
                else
                {
                    client.send(DStringUtils.generateJsonMessage(true, ticket.summary()));
                }
                break;
            }
            case "change_ticket": {
                if(!adminMiddleware(client)) break;
                long ticketId = readTicketId(client);
                if(ticketId == SQLModel.kInvalidIdentity) break;
                if(TicketUtils.changeTicketStatus(client.getUser(), ticketId, client.receive()))
                {
                    client.send(DStringUtils.generateJsonMessage(true, "Changed ticket status."));
                }
                else
                {
                    client.send(DStringUtils.generateJsonMessage(false, "Ticket assigned to someone else."));
                }
                break;
            }
        }
        client.flush();
    }

}
