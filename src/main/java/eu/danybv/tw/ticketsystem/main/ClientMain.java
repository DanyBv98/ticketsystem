package eu.danybv.tw.ticketsystem.main;

import eu.danybv.tw.ticketsystem.cli.framework.MenuStateMachine;
import eu.danybv.tw.ticketsystem.cli.menus.FrontPageMenu;
import eu.danybv.tw.ticketsystem.networking.Client;

import java.io.IOException;

public class ClientMain {
    public static void main(String[] args) throws IOException {
        var client = new Client();

        MenuStateMachine.push(new FrontPageMenu(client));
        while(!MenuStateMachine.show());


    }
}
