package eu.danybv.tw.ticketsystem.main;

import eu.danybv.tw.ticketsystem.DatabaseHelper;
import eu.danybv.tw.ticketsystem.networking.Server;

import java.io.IOException;

public class ServerMain {

    public static void main(String[] args) throws IOException {
        DatabaseHelper.createTables();
        var server = new Server();
        server.start();
    }

}
