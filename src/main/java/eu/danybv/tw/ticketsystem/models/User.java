package eu.danybv.tw.ticketsystem.models;

import eu.danybv.tw.ticketsystem.orm.SQLModel;
import eu.danybv.tw.ticketsystem.orm.annotations.*;
import org.jooq.impl.DSL;

import java.util.List;

@Model(tableName = "users")
public class User extends SQLModel {
    @DBProperty
    @Identity
    @PrimaryKey
    private long id = SQLModel.kInvalidIdentity;

    @DBProperty
    @NotNull
    private String username;

    @DBProperty(name = "pass")
    @NotNull
    private String password;

    @DBProperty
    @NotNull
    private boolean isAdmin = false;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public List<Ticket> getTickets() {
        Ticket sampleTicket = new Ticket();
        return sampleTicket.<Ticket>select(sampleTicket.selectQuery().where(DSL.field("userId").eq(id)));
    }

    @Override
    public String toString() {
        return id + " : " + username + " . Admin: " + isAdmin;
    }
}
