package eu.danybv.tw.ticketsystem.models;

import eu.danybv.tw.ticketsystem.orm.SQLModel;
import eu.danybv.tw.ticketsystem.orm.annotations.*;
import eu.danybv.tw.ticketsystem.orm.exceptions.NoTableNameSetException;
import org.jooq.impl.DSL;

@Model(tableName = "tickets")
public class Ticket extends SQLModel {
    @DBProperty
    @Identity
    @PrimaryKey
    private long id = SQLModel.kInvalidIdentity;

    @DBProperty
    private String title;

    @DBProperty
    private String description;

    @DBProperty
    private String status = "PENDING";

    @DBProperty
    @NotNull
    @ForeignKey(model = User.class, column = "id")
    private long userId;

    @DBProperty
    @ForeignKey(model = User.class, column = "id")
    private Long assignedUserId = null;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public User getUser() {
        var sampleUser = new User();
        var users = sampleUser.<User>select(sampleUser.selectQuery().where(DSL.field("id").eq(userId)));
        if (!users.isEmpty())
            return users.get(0);
        return null;
    }

    public User getAssignedUser() {
        if (assignedUserId == null) return null;
        var sampleUser = new User();
        var users = sampleUser.<User>select(sampleUser.selectQuery().where(DSL.field("id").eq(assignedUserId)));
        if (!users.isEmpty())
            return users.get(0);
        return null;
    }

    public void setUser(User user) {
        this.userId = user.getId();
    }

    public void setAssignedUser(User user) {
        this.assignedUserId = user.getId();
    }

    @Override
    public String toString() {
        String string = "Ticket \t#" + id + " | Subject: " + title + " | Opened by " + getUser().getUsername() + " | ";
        var assignedUser = getAssignedUser();
        if (assignedUser == null) string += "Not assigned | ";
        else string += "Assigned to " + assignedUser.getUsername() + " | ";
        string += "Status: " + status;
        return string;
    }

    public String summary() {
        var assignedUser = getAssignedUser();
        var ticketString = "Ticket #" + getId() + "\n";
        ticketString += "Subject: " + getTitle() + "\n";
        ticketString += "Description:\n" + getDescription() + "\n";
        ticketString += "Opened by: " + getUser().getUsername() + "\n";
        ticketString += "Assigned to: ";
        if(assignedUser == null) ticketString += "No one";
        else ticketString += assignedUser.getUsername();
        ticketString += "\n";
        ticketString += "Status: " + getStatus() + "\n";
        return ticketString;
    }
}
